"""
Panoramax client cli tool
"""

__version__ = "1.1.6-1"

USER_AGENT = f"PanoramaxCLI/{__version__}"
