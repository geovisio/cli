# ![Panoramax](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Panoramax.svg/40px-Panoramax.svg.png) Panoramax

__Panoramax__ is a digital resource for sharing and using 📍📷 field photos. Anyone can take photographs of places visible from the public streets and contribute them to the Panoramax database. This data is then freely accessible and reusable by all. More information available at [gitlab.com/panoramax](https://gitlab.com/panoramax) and [panoramax.fr](https://panoramax.fr/).


# ⌨️ Panoramax Command-line scripts

This repository contains only the __command-line interface (CLI)__ of Panoramax.

## Features

This tool allows you to:

- Upload pictures to a Panoramax server
- Download pictures from a Panoramax server
- Transfer pictures between Panoramax servers

## Documentation

All our documentation is available under [docs folder](https://gitlab.com/panoramax/clients/cli/-/tree/main/docs).

## ⚖️ License

Copyright (c) Panoramax team 2022-2024, [released under MIT license](https://gitlab.com/panoramax/clients/cli/-/blob/main/LICENSE).
